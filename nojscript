#!/usr/bin/env python3
"""
SETUP:
============================================================================
place this file under ~/.local/share/qutebrowser/userscripts/nojscript
AND make it EXECUTABLE
============================================================================
Paste the following into your cute config.py to enable permanent changes

# Import URL whitelist for nojscript
with open(str(config.configdir / 'jswhitelist'), 'r') as f:
    targets = f.readlines()
    count = 0
    while True:
        config.set('content.javascript.enabled', True, targets[count].rstrip())
        count = count + 1
        if count == len(targets):
            break
============================================================================
Add bindings in config.py
My bindings:

config.bind('Ah', 'spawn --userscript nojscript -t -u {url}')   #Toggles JS for the active URL temporarily
config.bind('Aa', 'spawn --userscript nojscript -t -e -x -m')   #Gives a dmenu prompt to select multiple URLs to toggle JS temporarily
config.bind('Ao', 'spawn --userscript nojscript -t -e -x')      #Gives a dmenu prompt to select one URL to toggle JS temporarily
config.bind('ah', 'spawn --userscript nojscript -u {url}')      #Toggles JS for the active URL permanently
config.bind('aa', 'spawn --userscript nojscript -e -x -m')      #Gives a dmenu prompt to select multiple URLs to toggle JS permanently
config.bind('ao', 'spawn --userscript nojscript -e -x')         #Gives a dmenu prompt to select one URL to toggle JS permanently
============================================================================
Create an empty whitelist:
    touch ~/.config/qutebrowser/jswhitelist
OR import your noscript whitelist:
    -export your data from noscript into noscript_data.txt (noscript builtin export function)
    -run: ~/.local/share/qutebrowser/userscripts/nojscript -W -i /path/to/noscript_data.txt
============================================================================
NOTE:
    ---it is a good idea to backup your jswhitelist file :)
    ---if your system doesn't have an emoji font installed, omit -e
    ---if your system doesn't use Xresources, omit -x
    ---if you don't want to use dmenu or use your build, specify -a /path/to/alternative, where alternative uses dmenu syntax
    ---if you don't want to use the default location for the whitelist (~/.config/qutebrowser/jswhitelist), specify a custom path with -w and change in config.py accordingly
"""
import argparse
import os
import subprocess
import sys

parser = argparse.ArgumentParser(description='Finds out what the javascript hosts for the active page are and gives a dmenu prompt to enable/disable per host')
parser.add_argument('--temporary', '-t', dest='temporary', action='store_true', default=False,
                    help="Make changes be temporary and lost upon restart of qutebrowser")
parser.add_argument('--emoji', '-e', dest='use_emoji', action='store_true', default=False,
                    help="Use emoji to indicate whether JS is already disabled for a host or not")
parser.add_argument('--url', '-u', dest='single_url', action='store',
                    help="Toggle JS enabled/disabled for the specified URL only")
parser.add_argument('--xrdb', '-x', dest='read_xrdb', action='store_true',
                    help="Read the colors using xrdb. Useful for systems using wal/pywal")
parser.add_argument('--multiple', '-m', dest='select_multiple', action='store_true', default=False,
                    help="Don't stop after operating on one URL")
parser.add_argument('--whitelist', '-w', dest='whitelist_location', action='store', default="/home/felix/.config/qutebrowser/jswhitelist",
                    help="Specify an alternative location for the whitelist file. Defaults to '~/.config/qutebrowser/jswhitelist'")
parser.add_argument('--alternative-menu', '-a', dest='menu_path', action='store', default='/usr/bin/dmenu',
                    help="Specify the path to a dmenu alternative. Has to use the same syntax")
parser.add_argument('--import', '-i', dest='import_path', action='store', default=None,
                    help="Specify the path to a database to append it to the whitelist. Supported: noscript_data.txt")
parser.add_argument('--create-whitelist', '-W', dest='create_whitelist', action='store_true', default=False,
                    help="Do not abort when the specified/ default whitelist doesn't exist, instead create it")

def get_substring_from_string(input_string, index_begin, index_end):
    # Outputs the part of input string that is specified by index_begin/ index_end
    # Puts every letter (between indexes) in a list and joins it into a string
    output_string_list = []
    for i in range(index_begin, index_end):
        output_string_list.append(input_string[i])
    return ''.join(output_string_list)

def make_string_with_newlines(url_list):
    # Returns the input list as a string, with a newline between every list entry
    for i in range(0, len(url_list)):
        url_list[i] = url_list[i].rstrip() + '\n'
    return ''.join(url_list)

def patternize(url):
    # Removes Protocol and 'www' and replaces them with wildcards according to chrome match patterns
    colon_index = url.find('://')
    if url.find('www', colon_index + 3, colon_index + 6) != -1:
        move_index = colon_index + 4
    else:
        move_index = colon_index
    new_url = '*://*.' + get_substring_from_string(url, move_index + 3, len(url))
    return new_url

def open_html():
    # Outputs raw html data of the site opened in qute in one string
    with open(os.getenv('QUTE_HTML')) as p:
        html_code = p.read()
    return html_code

def qute_do(cmds):
    # Sends all commands in the list cmds to qute
    with open(os.environ['QUTE_FIFO'], 'w') as qute_fifo:
        for i in range(0, len(cmds)):
            qute_fifo.write(cmds[i] + '\n')
        qute_fifo.flush()

def is_script(html, index):
    # Returns True if the Tag on index is a <script> tag
    global Script_tag_list
    if html.find('script', index, index + 7) != -1:
        return True
    else:
        return False

def get_tags(html):
    # Returns all script tags found in html as list
    script_tag_list = []
    pos = 0
    while True:
        index_start = html.find('<', pos)
        if index_start == -1:
            break
        index_end = html.find('>', pos + 1)
        if is_script(html, index_start) == True:
            script_tag_list.append(get_substring_from_string(html, index_start, index_end + 1))
        pos = index_end
    return script_tag_list

def strip_path(url):
    # Returns an URL with the path after the TLD replaced with a wildcard for chrome pattern matching
    slash_pos = url.find('/', 8)
    if slash_pos != -1:
        return get_substring_from_string(url, 0, slash_pos) + '/*'
    else:
        return url + '/*'

def process_tag_list(tag_list):
    # Returns a list containing all URLs from the <script> tags in tag_list
    url_list = []
    for index in range(0, len(tag_list)):
        index_start = tag_list[index].find('src=')
        if index_start != -1:
            # if <script> contains a src attribute and if that contains a URL
            # (not just a path), append the URL to the output list
            url_pos = tag_list[index].find('http', index_start + 5, index_start + 9)
            if url_pos != -1:
                end_pos = tag_list[index].find('"', url_pos)
                if end_pos == -1:
                    end_pos = tag_list[index].find("'", url_pos)
                url_list.append(get_substring_from_string(tag_list[index], url_pos, end_pos))
    for i in range(0, len(url_list)):
        url_list[i] = strip_path(url_list[i])
    return url_list

def add_host_url(url_list):
    # Inserts the URL of the currently open site into the beginning of url_list,
    # and removes, if existing, the old instance. Does nothing on qute special URLs
    host_url = os.getenv('QUTE_URL')
    if host_url.find('qute://', 0, 7) == -1:
        host_url_stripped = strip_path(host_url)
        if host_url_stripped in url_list:
            url_list.remove(host_url_stripped)
        url_list.insert(0, host_url_stripped)
    return url_list

def remove_duplicates(url_list):
    return list(set(url_list))

def check_if_in_whitelist(url, whitelist_location):
    # Returns True if the given URL exists in the given whitelist, False otherwise.
    with open(whitelist_location, 'r') as wl:
        whitelist = wl.read()
        url = patternize(url)
    if url in whitelist:
        return True
    else:
        return False

def prefix_urls(url_list, use_emoji, whitelist_location):
    # Prefix an URL with 'X' or 'O' (or '⛔' and '✔', if using emojis),
    # depending on whether it is whitelisted or not.
    # Not only indicates that to the user, but to anything else in this script.
    if use_emoji == True:
        prefix_blocked = '⛔ '
        prefix_unblocked = '✔ '
    else:
        prefix_blocked = 'X '
        prefix_unblocked = 'O '
    for index in range(0, len(url_list)):
        if check_if_in_whitelist(url_list[index], whitelist_location) == True:
            url_list[index] = prefix_unblocked + url_list[index]
        else:
             url_list[index] = prefix_blocked + url_list[index]
    return url_list

def unprefix(url):
    # Removes anything from input URL that comes before 'http',
    # used for removing prefix indicators
    http_index = url.find('http')
    return get_substring_from_string(url, http_index, len(url))

def toggle_prefix(dmenu_string, prefix_index):
    # Toggles the prefix of an URL in the dmenu input string
    if dmenu_string[prefix_index] == '⛔':
        new_prefix = '✔'
    elif dmenu_string[prefix_index] == '✔':
        new_prefix = '⛔'
    elif dmenu_string[prefix_index] == 'X':
        new_prefix = 'O'
    else:
        new_prefix = 'X'
    return get_substring_from_string(dmenu_string, 0, prefix_index) + new_prefix + get_substring_from_string(dmenu_string, prefix_index + 1, len(dmenu_string))

def xrdb_evaluation():
    # Returns a list containing the arguments for dmenu with colors from Xresources
    xrdb = subprocess.run(['/usr/bin/xrdb', '-query'], capture_output=True, text=True)
    xrdb_output = xrdb.stdout
    index = xrdb_output.find('.color0')
    color0 = get_substring_from_string(xrdb_output, index + 9, index + 16)
    index = xrdb_output.find('.color1')
    color1 = get_substring_from_string(xrdb_output, index + 9, index + 16)
    index = xrdb_output.find('.color15')
    color15 = get_substring_from_string(xrdb_output, index + 10, index + 17)
    args_list = ['-nb', color0, '-nf', color15, '-sb', color1, '-sf', color15, '-i', '-l', '20', '-fn', 'Monospace-18']
    return args_list

def ask_dmenu(url_list, dmenu_arguments, multiple):
    # Uses a dmenu promt to select url(s) and returns them in a list
    if multiple == True:
        url_list.append('Finish')
    url_list.append('Quit')
    dmenu_string = make_string_with_newlines(url_list)
    chosen_list = []
    while True:
        dmenu = subprocess.run(dmenu_arguments, input=dmenu_string, capture_output=True, text=True)
        chosen = dmenu.stdout
        if chosen == '' or chosen == 'Quit\n':
            sys.exit(0)
        if chosen == 'Finish\n':
            break
        if dmenu_string.find(chosen) == -1:
            raise ChildProcessError("Invalid dmenu item selected")
            sys.exit(1)
        chosen_list.append(chosen)
        if multiple == False:
            break
        dmenu_string = toggle_prefix(dmenu_string, dmenu_string.find(chosen))
    for i in range(0, len(chosen_list)):
        chosen_list[i] = chosen_list[i].rstrip()
    return chosen_list

def toggle_in_browser(url, temporary, currently_not_whitelisted):
    # Toggles js for the given URL in qute, reloads the page and shows the user a message
    # that says what was done
    if temporary == True:
        temp_or_perm = 'Temporarily'
    else:
        temp_or_perm = 'Permanently'
    if currently_not_whitelisted == True:
        action = ' enabled '
    else:
        action = ' disabled '
    qute_do(['config-cycle -p -t -u ' + url + ' content.javascript.enabled', 'reload', 'message-info "Nojscript: ' + temp_or_perm + action + 'Javascript for host:' + url +'"'])

def toggle_js(url, whitelist_location, not_in_file):
    # Adds URL to the whitelist or removes it if already in there
    if not_in_file == True:
        # Host was blocked and gets whitelisted
        with open(whitelist_location, 'r') as wl:
            wl_test = wl.readlines()
            if url + '\n' not in  wl_test:
                with open(whitelist_location, 'a') as whitelist:
                    whitelist.write(url + '\n')
                    whitelist.flush()
                    whitelist.close()
    else:
        # Host was whitelisted and gets removed from the whitelist
        with open(whitelist_location, 'r+') as whitelist:
            whitelist_raw = whitelist.read()
            wl_url_index = whitelist_raw.find(url)
            whitelist_raw = get_substring_from_string(whitelist_raw, 0, wl_url_index) + get_substring_from_string(whitelist_raw, wl_url_index + len(url) + 2, len(whitelist_raw))
            whitelist.seek(0)
            whitelist.write(whitelist_raw)
            whitelist.truncate()

def process_single_url(url, use_emoji, whitelist_location):
    # Returns input URL with path stripped and prefix
    url = strip_path(url.rstrip())
    return prefix_urls([url], use_emoji, whitelist_location)

def get_doc_name(path):
    # Remove anything from given path but the filename and return it
    while True:
        index = path.find('/')
        if index == -1:
            break
        else:
            path = get_substring_from_string(path, index + 1, len(path))
    return path

def trim_unneeded_chars(input_string):
    # Remove characters before/after an URL that was in noscript_data.txt
    string_index = input_string.find('"')
    if input_string[string_index + 1] == '§':
        if input_string[string_index + 2] == ':':
            string_index = string_index + 2
        else:
            string_index = string_index + 1
    if input_string[len(input_string) - 2] == ',':
        output_string = get_substring_from_string(input_string, string_index + 1, len(input_string) - 3)
    else:
        output_string = get_substring_from_string(input_string, string_index + 1, len(input_string) - 2)
    return output_string

def trim_unneeded(input_list):
    # Remove any line that doesn't contain URLs to whitelist from noscript_data.txt
    index_start = input_list.index('      "trusted": [\n') + 1
    index_end = input_list.index('      ],\n', index_start)
    output_list = []
    for i in range(index_start, index_end):
        output_list.append(trim_unneeded_chars(input_list[i]))
    return output_list

def patternize_import(input_list):
    # Format the URLs from input_list
    output_list = []
    for i in range(0, len(input_list)):
            output_list.append('*://*.' + strip_path(input_list[i]))
    return output_list

def remove_unneeded(input_list):
    # Removes same URL (with different/same prefix) entries until there is either one or none left
    input_list_pos = 0
    while True:
        if input_list_pos < len(input_list):
            search_string = get_substring_from_string(input_list[input_list_pos], 2, len(input_list[input_list_pos]))
            count = input_list_pos + 1
            while True:
                if count > len(input_list) - 1:
                    input_list_pos = input_list_pos + 1
                    break
                elif input_list[count].find(search_string, 2) != -1:
                    input_list.pop(count)
                    input_list.pop(input_list_pos)
                    break
                else:
                    count = count + 1
        else:
            break
    return input_list

"""
            --------------------------------------------------------------
            --------------------------------------------------------------
            MAIN
            --------------------------------------------------------------
            --------------------------------------------------------------
"""
def main():
    arguments = parser.parse_args()
    # Read Xresources
    if arguments.read_xrdb == True:
        dmenu_arguments = [arguments.menu_path] + xrdb_evaluation()
    else:
        dmenu_arguments = arguments.menu_path
    # Expand '~' in Path for whitelist, quit with status 1 if there is no whitelist file
    if arguments.whitelist_location.find('~', 0, 1) != -1:
        arguments.whitelist_location = os.getenv('HOME') + get_substring_from_string(arguments.whitelist_location, 1, len(arguments.whitelist_location))
    if arguments.temporary == False and os.path.isfile(arguments.whitelist_location) == False:
        if arguments.create_whitelist == False:
            raise FileNotFoundError("The whitelist-file at " + arguments.whitelist_location + " does not exist. Create it, specify an existing one with -w or use -W to create it")
            sys.exit(1)
        # If '-W' was specified, create an empty white list file if it doesn't exist
        else:
            with open(arguments.whitelist_location, 'w') as wl_open:
                wl_open.write('')
#"""
#            --------------------------------------------------------------
#            IMPORT MODE
#            -i PATH
#            Reads URL's from a supported file (only noscript_data.txt right now) and appends them to the current whitelist.
#            --------------------------------------------------------------
#"""
    if arguments.import_path != None:
        # Check for file existence and if file is of a supported format
        if get_doc_name(arguments.import_path) != 'noscript_data.txt':
            raise PermissionError("The import file has to be the 'noscript_data.txt' you get when you export your data in Noscript. Other files are not supported.")
        if arguments.import_path.find('~', 0, 1) != -1:
            arguments.import_path = os.getenv('HOME') + get_substring_from_string(arguments.import_path, 1, len(arguments.import_path))
        if os.path.isfile(arguments.import_path) == False:
            raise FileNotFoundError("The import file at " + arguments.import_path + " does not exist.")
        # Put file content into a list
        with open(arguments.import_path, 'r') as import_file:
            import_list = import_file.readlines()
        # Format the URLs
        urls_raw = trim_unneeded(import_list)
        url_patterns = patternize_import(urls_raw)

        # Append the URLs to whitelist file (if not in there already)
        for i in range(0, len(url_patterns)):
            toggle_js(url_patterns[i], arguments.whitelist_location, True)

#"""
#            --------------------------------------------------------------
#            NORMAL OPERATION MODE
#            Reads qute's currently open html page and puts every URL that is part of a <script> tag in a list
#            --------------------------------------------------------------
#"""
    elif not arguments.single_url:
        # Get URLs from current open page in qute
        html = open_html()
        html_tag_list = get_tags(html)
        url_list = process_tag_list(html_tag_list)
        # Remove duplicates
        url_list = remove_duplicates(url_list)
        # Add URL of current open page, if not in url_list already
        url_list = add_host_url(url_list)
        # Quit if url_list is empty
        if len(url_list) == 0:
            sys.exit(0)
        # Prefix URLs with emoji/ 'X'/'O' to indicate blocked status. Also used for later parts to see if an URL is whitelisted already.
        url_list = prefix_urls(url_list, arguments.use_emoji, arguments.whitelist_location)
        # Prompt user for URLs to toggle, multiple choices are possible if using '-m'
        chosen_list = ask_dmenu(url_list, dmenu_arguments, arguments.select_multiple)
#"""
#            --------------------------------------------------------------
#            SINGLE URL PROCESSING
#            -u URL
#            Puts only the specified URL into a list.
#            --------------------------------------------------------------
#"""
    else:
        chosen_list = process_single_url(arguments.single_url, arguments.use_emoji, arguments.whitelist_location)
#"""
#            --------------------------------------------------------------
#            FORMAT URLs
#            --------------------------------------------------------------
#"""
    # If multiple URLs were selectable, remove entries that cancel each other out to prevent unnecessary action
    if arguments.select_multiple == True:
        chosen_list = remove_unneeded(chosen_list)
    # Format each URL as a pattern
    url_patterns = []
    for i in range(0, len(chosen_list)):
        url_pattern = patternize(unprefix(chosen_list[i]))
        if chosen_list[i][0] in ['X', '⛔']:
            not_whitelisted = True
        else:
            not_whitelisted = False
#"""
#            --------------------------------------------------------------
#            WRITE CHANGES TO FILE/ QUTEBROWSER
#            --------------------------------------------------------------
#"""
        # Append to whitelist file or remove from (if not in temporary mode), tell qutebrowser to enable/disable js and reload the page
        if arguments.temporary == False:
            toggle_js(url_pattern, arguments.whitelist_location, not_whitelisted)
        toggle_in_browser(url_pattern, arguments.whitelist_location, not_whitelisted)


if __name__ == "__main__":
    main()
    sys.exit(0)
