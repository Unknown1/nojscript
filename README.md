# nojscript

Qutebrowser userscript that can see what URLs are hosting JavaScript on a given site and gives the user a dmenu prompt to toggle JS for each of those sites.

Kind of like what NoScript does, just for deactivating JavaScript only.

# Setup

* Place this file in your qutebrowser userscripts directory `~/.local/share/qutebrowser/userscripts/nojscript`
* Make it executable: `chmod +x ~/.local/share/qutebrowser/userscripts/nojscript`
* Add the following lines to your config.py:`with open(str(config.configdir / 'jswhitelist'), 'r') as f:
    targets = f.readlines()
    count = 0
    while True:
        config.set('content.javascript.enabled', True, targets[count].rstrip())
        count = count + 1
        if count == len(targets):
            break" >> ~/.config/qutebrowser/config.py`
* Disable JavaScript for qutebrowser in config.py:
  `c.content.javascript.enabled = False`
* Create a whitelist file: `touch ~/.config/qutebrowser/jswhitelist`
* Or import your noscript data: export data in noscript and run `~/.local/share/qutebrowser/userscripts/nojscript -W -i /path/to/result/noscript_data.txt`
* Add bindings to your config.py. My bindings:
* 	`config.bind('Ah', 'spawn --userscript nojscript -t -u {url}')	#Toggles JS for the active URL temporarily`
* 	`config.bind('Aa', 'spawn --userscript nojscript -t -e -x -m')   #Gives a dmenu prompt to select multiple URLs to toggle JS temporarily`
* 	`config.bind('Ao', 'spawn --userscript nojscript -t -e -x')      #Gives a dmenu prompt to select one URL to toggle JS temporarily`
* 	`config.bind('ah', 'spawn --userscript nojscript -u {url}')	#Toggles JS for the active URL permanently`
* 	`config.bind('aa', 'spawn --userscript nojscript -e -x -m')      #Gives a dmenu prompt to select multiple URLs to toggle JS permanently`
* 	`config.bind('ao', 'spawn --userscript nojscript -e -x')         #Gives a dmenu prompt to select one URL to toggle JS permanently`

# Options
nojscript currently has the following options:

*  `-a --alternative-menu /path/to/menu/executable` Specify the path to an alternative menu executable or own dmenu build. Has to understand dmenu syntax
*  `-e --emoji` Use emoji to indicate whether an URL has JS disabled or not rather than 'X'(disabled) and 'O'(enabled). Don't use if you have no emoji font installed
*  `-i --import /path/to/noscript_data.txt` Put all URLs that were whitelisted in NoScript into the whitelist
*  `-m --multiple` Select more than one URL
*  `-t --temporary` Make all changes be lost upon closing qutebrowser and don't write anything to the whitelist
*  `-u --url URL` Make nojscript operate on URL only and don't give a menu
*  `-w --whitelist /path/to/whitelist` Specify the path to a whitelist file. If not specified, '~/.config/qutebrowser/jswhitelist' is used
*  `-W --create-whitelist` If specified, nojscript will not give an error if the whitelist file is not found, it will instead create an empty one and continue
*  `-x --xrdb` Use Xresources to color dmenu like your system

# Note
This is the first useful thing I ever wrote (aside from a few shell scripts), so don't expect any coding quality, efficiency, good style or readability.

However, I would greatly appreciate your feedback (positive or constructive =) ), ideas for features and help/advice so I can become a better programmer and improve at coding.
